# Introduction to clojure-basics/functional programming

This project record some basics about **Clojure**.

## Overview

Clojure is a functional programing language, which means `functions` are first-class.What does that means? This means eveythings besides prime types or values object, are functions, unlike OO language, treat `everything` is an object, there are many controversial between these two programming paradigms, which is very interesting but I don't want to address too much here.

```clojure
;; Function Defination
;;    name   params         body
;;    -----  ------  -------------------
(defn greet  [name]  (str "Hello, " name) )
```

```clojure
;; Function call/Evaluation
;;    name   params         body
;;    -----  ------  -------------------
	(greet "foo-bar")
```

## Types & functions
Basic types are defines [here](https://clojure.org/guides/learn/syntax)
Some method about how to defines functions: https://clojure.org/guides/learn/function
	
	`Locals and Closures`

### There is no `if/else` or `for-loop`
Why using if/else, for-loop

#### Is this overly complex?

### Everything is a function call? Macro come to rescue.

When we type `defn` the REPL shows macro this mean defn is a macro,
we check the source by eval source

```clojure
(source defn)
```
```
(def 
^{:doc "Same as (def name (fn [params* ] exprs*)) or (def
    name (fn ([params* ] exprs*)+)) with any doc-string or attrs added
    to the var metadata. prepost-map defines a map with optional keys
    :pre and :post that contain collections of pre or post conditions."
   :arglists '([name doc-string? attr-map? [params*] prepost-map? body]
                [name doc-string? attr-map? ([params*] prepost-map? body)+ attr-map?])
   :added "1.0"}
 defn (fn defn [&form &env name & fdecl]
        ;; Note: Cannot delegate this check to def because of the call to (with-meta name ..)
        (if (instance? clojure.lang.Symbol name)
          nil
          (throw (IllegalArgumentException. "First argument to defn must be a symbol")))
        (let [m (if (string? (first fdecl))
                  {:doc (first fdecl)}
                  {})
              fdecl (if (string? (first fdecl))
                      (next fdecl)
                      fdecl)
              m (if (map? (first fdecl))
                  (conj m (first fdecl))
                  m)
              fdecl (if (map? (first fdecl))
                      (next fdecl)
                      fdecl)
              fdecl (if (vector? (first fdecl))
                      (list fdecl)
                      fdecl)
              m (if (map? (last fdecl))
                  (conj m (last fdecl))
                  m)
              fdecl (if (map? (last fdecl))
                      (butlast fdecl)
                      fdecl)
              m (conj {:arglists (list 'quote (sigs fdecl))} m)
              m (let [inline (:inline m)
                      ifn (first inline)
                      iname (second inline)]
                  ;; same as: (if (and (= 'fn ifn) (not (symbol? iname))) ...)
                  (if (if (clojure.lang.Util/equiv 'fn ifn)
                        (if (instance? clojure.lang.Symbol iname) false true))
                    ;; inserts the same fn name to the inline fn if it does not have one
                    (assoc m :inline (cons ifn (cons (clojure.lang.Symbol/intern (.concat (.getName ^clojure.lang.Symbol name) "__inliner"))
                                                     (next inline))))
                    m))
              m (conj (if (meta name) (meta name) {}) m)]
          (list 'def (with-meta name m)
                ;;todo - restore propagation of fn name
                ;;must figure out how to convey primitive hints to self calls first
                                                                ;;(cons `fn fdecl)
                                                                (with-meta (cons `fn fdecl) {:rettag (:tag m)})))))
```

## [The **macro** is just for sytax sugar?](http://www.phyast.pitt.edu/~micheles/scheme/scheme12.html)

## Function Composition
## [basic](https://bartoszmilewski.com/2014/11/04/category-the-essence-of-composition/)
(fg)(x) = f(g(x))


### Funtors
def F s.t (F.f).(F.g)(x) = f.g(F(x)) 

Datatypes

The List Functor

### Monoid
https://bartoszmilewski.com/2016/11/21/monads-programmers-definition/


### Better programming
https://www.cs.cornell.edu/courses/cs3110/2019sp/textbook/intro/pl.html

