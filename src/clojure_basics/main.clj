(ns disk_tool
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

(def cli-options
  ;; an option with a required argument
  [["-fp" "--file-path"]
   ["-h" "--help"]])

(defn -main [& args]
  (parse-opts args cli-options))
